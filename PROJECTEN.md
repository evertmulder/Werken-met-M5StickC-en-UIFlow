#  Projecten met M5StickC en UIFlow
_Toepassingen met M5StickC, Python, UI Flow en Arduino_

Door de populariteit van Arduino, de ESP32 microcontroller en de M5StickC zijn er diverse projecten via het internet beschikbaar gesteld. Op deze pagina een hommage.

## [Nixie klok op M5StickC](https://github.com/McOrts/M5StickC_Nixie_tube_Clock)
<img src="/others_artwork/m5stickc_nixie_clock_macsbug.jpeg" align="left" width="100">Dit project maakt van de M5StickC een horloge met nixiebuisjes. Het is een in Arduino geschreven Open Source programma dat voor de afbeelding van de nixiebuisjes gebruik maakt van een handvol afbeeldingen. Voor ieder tijdstip worden de juiste afbeeldingen op het scherm gezet. Het effect is een indrukwekkende, echt gelijkende klok dat als blikvanger of demonstratie dienst kan doen.

De re-creatie van dit project vereist het installeren van de Arduino programmeeromgeving, het installeren van de ESP32 bibliotheken en de M5StickC ondersteuning, het downloaden van dit project en het uploaden ervan naar de M5StickC. (_Via [macsbug](https://macsbug.wordpress.com/2019/06/06/m5stickc-nixie-tube-clock/)_).

## [Communiceren met Chirp](https://m5stack.hackster.io/H0meMadeGarbage/chirp-with-m5stickc-00f935)
<img src="/others_artwork/chirp_m5stickc_hmg.jpeg" align="left" width="100">Naast bluetooth en wifi zijn er meer manieren om twee microcontrollers met elkaar te laten communiceren. Een zeer tot de (Star Wars, R2D2) verbeelding sprekende manier is communicatie met fluittoontjes, _chirps_. Het project dat hiervan gebruik maakt op de M5StickC laat tekst invoeren op een smartphone, die met de 'chirps' naar de M5StickC wordt verzonden, waarna de ontvangen geluiden als tekst op het scherm worden getoverd. (_Via [hackster.io/H0meMadeGarbage](https://m5stack.hackster.io/H0meMadeGarbage)_).

## [Digitale hoekmeter](https://m5stack.hackster.io/shasha-liu/diy-protractor-with-m5stickc-e67e88)
<img src="/others_artwork/m5stickc_uiflow_angle_meter_shasha_liu.jpeg" align="left" width="100">Een heel aardige demonstratie van de M5StickC met een Grove sensor is het gebruik als digitale hoekmeter. Met een plexiglas uitbreiding is zo'n hoekmeter goed bruibaar in de bouw of in de DHZ-hobby. Het project bestaat uit een M5StickC, een hoeksensor en een setje plexiglas hulpstukken voor de montage. De software is geschreven in UI Flow/MicroPython en het geheel is goed en eenvoudig te reproduceren. (_Via [hackster.io/Shasha Liu](https://m5stack.hackster.io/shasha-liu)_).